<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <title>HTML/CSS/JavaScript学習</title>
    </head>
    <body>
       <div id="contents">
          <h1>学習成果一覧</h1>
          <div>
<?php

$type= [
    '01_HTML'            => 13,
    '02_CSS'             => 24,
    '03_JavaScript'      => 23,
    '04_JavaScriptCanvas'=> 2,
    '05_Game1' =>3,
    '06_Game2' =>3,
    '07_Game3' =>3,
    '08_Phina' =>3,
    '09_Block' =>3,
];

foreach ($type as $key=>$repeat){
    view($key, $repeat);
}

function view($type, $repeat){
    echo '<h2>'.$type.'</h2>';
    for($i=1;$i<=$repeat;$i++){
        $file     = 'ct1/'.$type.'/work'.sprintf("%02d", $i).'.html';
        $linkname = 'work'.sprintf("%02d", $i).'.html';
        if (file_exists(dirname(__FILE__).'/'.$file)){
            echo '[<a href='.$file .'>'.$linkname.'</a>] ';
        }else{
            echo '['.$linkname.'] ';
        }
        echo " ";
    }
    echo '<hr>';
}
